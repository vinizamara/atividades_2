//Maior número
/*
Crie um algoritmo que solicite três números 
do usuário e exiba o maior deles.
*/

namespace exercicio_6
{
    let numero1, numero2, numero3: number;
    numero1 = 5;
    numero2 = 2;
    numero3 = 1;

    if (numero3 > numero2 && numero3 > numero1)
    {
        console.log(`O maior número é: ${numero3}`);
    }

    else if (numero2 > numero3 && numero2 > numero1)
    {
        console.log(`O maior número é: ${numero2}`)
    }

    else
    {
        console.log(`O maior número é: ${numero1}`)
    }
}
