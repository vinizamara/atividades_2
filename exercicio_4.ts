//Número par ou ímpar
/*
Faça um programa que recebe um número inteiro
e verifique se esse número é par ou ímpar.
*/

namespace exercicio_4
{
    let numero, divisao: number;
    numero = 2;
    divisao = numero % 2;

    if (divisao == 0)
    {
        console.log("O número é par");
    }

    else
    {
        console.log("O número é ímpar");
    }
}
