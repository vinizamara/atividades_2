//Média ponderada
/*
Faça um programa que receba as três notas, calcule
e mostre amédia ponderada e o conceito que segue a 
tabela abaixo:
*/

namespace exercicio_2
{
    let nota1, nota2, nota3, media: number;
    nota1 = 1;
    nota2 = 6;
    nota3 = 10;

    media = (nota1 * 2 + nota2 * 3 + nota3 * 5) / 10;

    console.log(media);

    if (media >= 8 && media <=10)
    {
        console.log("Conceito A");
    }

    else if (media >= 7 && media < 8)
    {
        console.log("Conceito B");
    }

    else if (media >= 6 && media < 7)
    {
        console.log("Conceito C");
    }

    else if (media >= 5 && media < 6)
    {
        console.log("Conceito D");
    }

    else
    {
        console.log("Conceito E");
    }
}
